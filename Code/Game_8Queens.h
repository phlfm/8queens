#pragma once
/**
Game Rules:
	- Simple game engine that lets the player put pieces in valid locations
	- Once N pieces have been put the player wins
		- Game piece can be pawn, rook, knight, bishop, queen, king or a mixture of them
		- N can be 1 to ...
		- Board can be 1x1 to NxM
	- The game does NOT check if a solution is possible (10 queens in a 1x1 board will be OK)
	- Player will be an EXTERNAL (to this class) user programmed AI algorithm
/**/
class Game_8Queens
{
public:
	Game_8Queens();
	~Game_8Queens();

private:

};

